# Q – Rainer Schwarzbach’s Text Utilities

_Test conversion and transcoding utilities_


## Abstract

This package provides the module

- **reduce**: reduce Latin text in Unicode to ASCII(-encodable unicode text)


## Installation

qrstu is available on PyPI: <https://pypi.org/project/qrstu/>

```
pip install qrstu
```

Installation in a virtual environment is strongly recommended.


## Usage


### reduce

The **reduce** module can be used to reduce Unicode text
in Latin script to ASCII encodable Unicode text,
similar to **[Unidecode](https://pypi.org/project/Unidecode/)**
but taking a different approach
(ie. mostly wrapping functionality from the standard library module
**[unicodedata](https://docs.python.org/3/library/unicodedata.html)**).
Unlike **Unidecode** which also transliterates characters from non-Latin scripts,
**reduce** stubbornly refuses to handle these.

You can, however, specify an optional `errors=` argument in the
**reduce.reduce_text()** call, which is passed to the internally used
**[codecs.encode()](https://docs.python.org/3/library/codecs.html#codecs.encode)**
function, thus taking advance of the codecs module errors handling.

```python
>>> from qrstu import reduce
>>> # Vietnamese text
>>> reduce.reduce_text("Chào mừng đến với Hà Nội!")
'Chao mung dhen voi Ha Noi!'
>>>
>>> # Trying the Unidecode examples …
>>> reduce.reduce_text('kožušček')
'kozuscek'
>>> reduce.reduce_text('30 \U0001d5c4\U0001d5c6/\U0001d5c1')
'30 km/h'
>>> reduce.reduce_text('\u5317\u4EB0')
Traceback (most recent call last):
  File "…/qrstu/src/qrstu/reduce.py", line 354, in reduce_text
    chunk = translations[character.nfc]
            ~~~~~~~~~~~~^^^^^^^^^^^^^^^
KeyError: '北'

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "…/qrstu/src/qrstu/reduce.py", line 276, in reduce
    collector.append(PRESET_CHARACTER_REDUCTIONS[codepoint])
                     ~~~~~~~~~~~~~~~~~~~~~~~~~~~^^^^^^^^^^^
KeyError: 21271

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "…/qrstu/src/qrstu/reduce.py", line 356, in reduce_text
    chunk = character.reduce(errors=errors)
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "…/qrstu/src/qrstu/reduce.py", line 278, in reduce
    encoded = codecs.encode(
              ^^^^^^^^^^^^^^
UnicodeEncodeError: 'ascii' codec can't encode character '\u5317' in position 0: ordinal not in range(128)
>>> reduce.reduce_text('\u5317\u4EB0', errors="ignore")
''
>>> reduce.reduce_text('\u5317\u4EB0', errors="replace")
'??'
>>> reduce.reduce_text('\u5317\u4EB0', errors="backslashreplace")
'\\u5317\\u4eb0'
>>> reduce.reduce_text('\u5317\u4EB0', errors="xmlcharrefreplace")
'&#21271;&#20144;'
>>> reduce.reduce_text('\u5317\u4EB0', errors="namereplace")
'\\N{CJK UNIFIED IDEOGRAPH-5317}\\N{CJK UNIFIED IDEOGRAPH-4EB0}'
>>>
```

### transcode

The **transcode** module can be used to encode and decode strings
from and to byte sequences.

The **transcode.detect\_encoding()** function tries to autodetect an encoding
either from a [BOM](https://en.wikipedia.org/wiki/Byte_order_mark)
or the first of a given list of codecs that succeeds in decoding the
provided byte sequence to a unicode string.

The **transcode.to\_unicode()** function decodes a bytes sequence to a unicode string
by trying the following in sequence:

- any encoding determined by a BOM (ie. UTF-16 or UTF-32 with BOM,
  or [UTF-8 with signature](https://en.wikipedia.org/wiki/UTF-8#Byte_order_mark))
- any additional encoding provided as positional argument after the bytes sequence
- [ASCII](https://en.wikipedia.org/wiki/ASCII)
- **[UTF-8](https://en.wikipedia.org/wiki/UTF-8)**
- the fallback 8-bit encoding specified using the
  _fallback\_encoding_ keyword argument
  (default: [CP-1252](https://en.wikipedia.org/wiki/Windows-1252))

The first successfully decoded unicode string is returned.

Some impressions from the **transcode** module:

```python
>>> from qrstu import transcode
>>> source_text = "Árvíztűrő tükörfúrógép"
>>> latin_2_bytes = transcode.to_bytes(source_text, to_encoding="iso8859-2")
>>> latin_2_bytes
b'\xc1rv\xedzt\xfbr\xf5 t\xfck\xf6rf\xfar\xf3g\xe9p'
>>> transcode.detect_encoding(latin_2_bytes, "ascii", "utf-8")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "…/qrstu/src/qrstu/transcode.py", line 125, in detect_encoding
    raise DetectionFailedError(checked_encodings)
qrstu.transcode.DetectionFailedError: ['utf_32_be', 'utf_32_le', 'utf_16_be', 'utf_16_le', 'utf_8_sig', 'ascii', 'utf-8']
>>> transcode.detect_encoding(latin_2_bytes, "ascii", "utf-8", "iso8859-2")
EncodingDetectionResult(source=b'\xc1rv\xedzt\xfbr\xf5 t\xfck\xf6rf\xfar\xf3g\xe9p', unicode='Árvíztűrő tükörfúrógép', detected_encoding='iso8859-2')
>>> 
>>> utf_8_bytes = transcode.to_bytes(source_text, to_encoding="utf-8")
>>> transcode.detect_encoding(utf_8_bytes, "ascii", "utf-8", "iso8859-2")
EncodingDetectionResult(source=b'\xc3\x81rv\xc3\xadzt\xc5\xb1r\xc5\x91 t\xc3\xbck\xc3\xb6rf\xc3\xbar\xc3\xb3g\xc3\xa9p', unicode='Árvíztűrő tükörfúrógép', detected_encoding='utf-8')
>>> 
>>> transcode.to_unicode(utf_8_bytes)
'Árvíztűrő tükörfúrógép'
>>> 
>>> transcode.to_unicode(latin_2_bytes)
'Árvíztûrõ tükörfúrógép'
>>> transcode.to_unicode(latin_2_bytes, fallback_encoding="iso8859-2")
'Árvíztűrő tükörfúrógép'
>>> 
```
