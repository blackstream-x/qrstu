# -*- coding: utf-8 -*-

"""

tests.commons

Common functionality for Unit tests


Copyright (C) 2023 Rainer Schwarzbach

This file is part of qrstu.

qrstu is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

qrstu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""


import io
import sys

from typing import Dict, List
from unittest.mock import patch


RETURNCODE_OK = 0

ERRORS_IGNORE = "ignore"
ERRORS_STRICT = "strict"

ASCII = "ascii"
CP1252 = "cp1252"
ISO8859_2 = "iso8859_2"
ISO8859_5 = "iso8859_5"
ISO8859_9 = "iso8859_9"
ISO8859_10 = "iso8859_10"
UTF_8 = "utf-8"
UTF_8_SIG = "utf_8_sig"

GUESS_DATA: Dict[str, List[str]] = {
    CP1252: [
        "Falsches Üben von Xylophonmusik quält jeden größeren Zwerg",
        "Victor jagt zwölf Boxkämpfer quer über den großen Sylter Deich",
        "Voyez le brick géant que j’examine près du wharf",
        "Dès Noël où un zéphyr haï me vêt de glaçons würmiens,"
        " je dîne d’exquis rôtis de bœuf au kir à l’aÿ d’âge mûr & cætera !",
        "Ré só que vê galã sexy pôr kiwi talhado à força em baú"
        " põe juíza má em pânico",
        "Benjamín pidió una bebida de kiwi y fresa. Noé, sin vergüenza,"
        " la más exquisita champaña del menú",
    ],
    ISO8859_2: [
        "Stróż pchnął kość w quiz gędźb vel fax myjń",
        "Kŕdeľ šťastných ďatľov učí pri ústí Váhu mĺkveho koňa obhrýzať"
        " kôru a žrať čerstvé mäso",
        "Nechť již hříšné saxofony ďáblů rozezvučí síň úděsnými tóny waltzu,"
        " tanga a quickstepu",
        "V kožuščku hudobnega fanta stopiclja mizar in kliče",
        "Árvíztűrő tükörfúrógép",
    ],
    ISO8859_5: [
        "Съешь ещё этих мягких французских булок, да выпей чаю",
    ],
    ISO8859_9: [
        "Pijamalı hasta yağız şoföre çabucak güvendi",
    ],
    ISO8859_10: [
        "Høj bly gom vandt fræk sexquiz på wc",
        "Flygande bäckasiner söka hwila på mjuka tuvor",
        "Kæmi ný öxi hér, ykist þjófum nú bæði víl og ádrepa",
    ],
}

DETERMINISTIC_DATA: Dict[str, Dict[str, bytes]] = {
    "Falsches Üben von Xylophonmusik quält jeden größeren Zwerg": {
        UTF_8: b"Falsches \xc3\x9cben von Xylophonmusik qu\xc3\xa4lt"
        b" jeden gr\xc3\xb6\xc3\x9feren Zwerg",
        CP1252: b"Falsches \xdcben von Xylophonmusik qu\xe4lt"
        b" jeden gr\xf6\xdferen Zwerg",
    },
    "Victor jagt zwölf Boxkämpfer quer über den großen Sylter Deich": {
        UTF_8: b"Victor jagt zw\xc3\xb6lf Boxk\xc3\xa4mpfer"
        b" quer \xc3\xbcber den gro\xc3\x9fen Sylter Deich",
        CP1252: b"Victor jagt zw\xf6lf Boxk\xe4mpfer"
        b" quer \xfcber den gro\xdfen Sylter Deich",
    },
    "Voyez le brick géant que j’examine près du wharf": {
        UTF_8: b"Voyez le brick g\xc3\xa9ant"
        b" que j\xe2\x80\x99examine pr\xc3\xa8s du wharf",
        CP1252: b"Voyez le brick g\xe9ant que j\x92examine pr\xe8s du wharf",
    },
    "Dès Noël où un zéphyr haï me vêt de glaçons würmiens,"
    " je dîne d’exquis rôtis de bœuf au kir à l’aÿ d’âge mûr & cætera !": {
        UTF_8: b"D\xc3\xa8s No\xc3\xabl o\xc3\xb9 un z\xc3\xa9phyr"
        b" ha\xc3\xaf me v\xc3\xaat de gla\xc3\xa7ons w\xc3\xbcrmiens,"
        b" je d\xc3\xaene d\xe2\x80\x99exquis r\xc3\xb4tis de b\xc5\x93uf"
        b" au kir \xc3\xa0 l\xe2\x80\x99a\xc3\xbf d\xe2\x80\x99\xc3\xa2ge"
        b" m\xc3\xbbr & c\xc3\xa6tera !",
        CP1252: b"D\xe8s No\xebl o\xf9 un z\xe9phyr"
        b" ha\xef me v\xeat de gla\xe7ons w\xfcrmiens,"
        b" je d\xeene d\x92exquis r\xf4tis de b\x9cuf au kir"
        b" \xe0 l\x92a\xff d\x92\xe2ge m\xfbr & c\xe6tera !",
    },
    "Ré só que vê galã sexy pôr kiwi talhado à força"
    " em baú põe juíza má em pânico": {
        UTF_8: b"R\xc3\xa9 s\xc3\xb3 que v\xc3\xaa gal\xc3\xa3 sexy"
        b" p\xc3\xb4r kiwi talhado \xc3\xa0 for\xc3\xa7a"
        b" em ba\xc3\xba p\xc3\xb5e ju\xc3\xadza m\xc3\xa1 em p\xc3\xa2nico",
        CP1252: b"R\xe9 s\xf3 que v\xea gal\xe3 sexy p\xf4r kiwi talhado"
        b" \xe0 for\xe7a em ba\xfa p\xf5e ju\xedza m\xe1 em p\xe2nico",
    },
    "Benjamín pidió una bebida de kiwi y fresa. Noé, sin vergüenza,"
    " la más exquisita champaña del menú": {
        UTF_8: b"Benjam\xc3\xadn pidi\xc3\xb3 una bebida de kiwi y fresa."
        b" No\xc3\xa9, sin verg\xc3\xbcenza,"
        b" la m\xc3\xa1s exquisita champa\xc3\xb1a del men\xc3\xba",
        CP1252: b"Benjam\xedn pidi\xf3 una bebida de kiwi y fresa."
        b" No\xe9, sin verg\xfcenza, la m\xe1s exquisita"
        b" champa\xf1a del men\xfa",
    },
    "Stróż pchnął kość w quiz gędźb vel fax myjń": {
        UTF_8: b"Str\xc3\xb3\xc5\xbc pchn\xc4\x85\xc5\x82"
        b" ko\xc5\x9b\xc4\x87 w quiz g\xc4\x99d\xc5\xbab vel fax myj\xc5\x84",
        ISO8859_2: b"Str\xf3\xbf pchn\xb1\xb3 ko\xb6\xe6"
        b" w quiz g\xead\xbcb vel fax myj\xf1",
    },
    "Kŕdeľ šťastných ďatľov učí pri ústí Váhu"
    " mĺkveho koňa obhrýzať kôru a žrať čerstvé mäso": {
        UTF_8: b"K\xc5\x95de\xc4\xbe \xc5\xa1\xc5\xa5astn\xc3\xbdch"
        b" \xc4\x8fat\xc4\xbeov u\xc4\x8d\xc3\xad pri \xc3\xbast\xc3\xad"
        b" V\xc3\xa1hu m\xc4\xbakveho ko\xc5\x88a obhr\xc3\xbdza\xc5\xa5"
        b" k\xc3\xb4ru a \xc5\xbera\xc5\xa5 \xc4\x8derstv\xc3\xa9 m\xc3\xa4so",
        ISO8859_2: b"K\xe0de\xb5 \xb9\xbbastn\xfdch \xefat\xb5ov"
        b" u\xe8\xed pri \xfast\xed V\xe1hu m\xe5kveho ko\xf2a obhr\xfdza\xbb"
        b" k\xf4ru a \xbera\xbb \xe8erstv\xe9 m\xe4so",
    },
    "Nechť již hříšné saxofony ďáblů rozezvučí"
    " síň úděsnými tóny waltzu, tanga a quickstepu": {
        UTF_8: b"Nech\xc5\xa5 ji\xc5\xbe h\xc5\x99\xc3\xad\xc5\xa1n\xc3\xa9"
        b" saxofony \xc4\x8f\xc3\xa1bl\xc5\xaf rozezvu\xc4\x8d\xc3\xad"
        b" s\xc3\xad\xc5\x88 \xc3\xbad\xc4\x9bsn\xc3\xbdmi t\xc3\xb3ny waltzu,"
        b" tanga a quickstepu",
        ISO8859_2: b"Nech\xbb ji\xbe h\xf8\xed\xb9n\xe9 saxofony"
        b" \xef\xe1bl\xf9 rozezvu\xe8\xed s\xed\xf2 \xfad\xecsn\xfdmi"
        b" t\xf3ny waltzu, tanga a quickstepu",
    },
    "V kožuščku hudobnega fanta stopiclja mizar in kliče": {
        UTF_8: b"V ko\xc5\xbeu\xc5\xa1\xc4\x8dku hudobnega fanta"
        b" stopiclja mizar in kli\xc4\x8de",
        ISO8859_2: b"V ko\xbeu\xb9\xe8ku hudobnega fanta"
        b" stopiclja mizar in kli\xe8e",
    },
    "Árvíztűrő tükörfúrógép": {
        UTF_8: b"\xc3\x81rv\xc3\xadzt\xc5\xb1r\xc5\x91"
        b" t\xc3\xbck\xc3\xb6rf\xc3\xbar\xc3\xb3g\xc3\xa9p",
        ISO8859_2: b"\xc1rv\xedzt\xfbr\xf5 t\xfck\xf6rf\xfar\xf3g\xe9p",
    },
    "Съешь ещё этих мягких французских булок, да выпей чаю": {
        UTF_8: b"\xd0\xa1\xd1\x8a\xd0\xb5\xd1\x88\xd1\x8c"
        b" \xd0\xb5\xd1\x89\xd1\x91 \xd1\x8d\xd1\x82\xd0\xb8\xd1\x85"
        b" \xd0\xbc\xd1\x8f\xd0\xb3\xd0\xba\xd0\xb8\xd1\x85"
        b" \xd1\x84\xd1\x80\xd0\xb0\xd0\xbd\xd1\x86\xd1\x83\xd0\xb7\xd1\x81"
        b"\xd0\xba\xd0\xb8\xd1\x85 \xd0\xb1\xd1\x83\xd0\xbb\xd0\xbe\xd0\xba,"
        b" \xd0\xb4\xd0\xb0 \xd0\xb2\xd1\x8b\xd0\xbf\xd0\xb5\xd0\xb9"
        b" \xd1\x87\xd0\xb0\xd1\x8e",
        ISO8859_5: b"\xc1\xea\xd5\xe8\xec \xd5\xe9\xf1 \xed\xe2\xd8\xe5"
        b" \xdc\xef\xd3\xda\xd8\xe5"
        b" \xe4\xe0\xd0\xdd\xe6\xe3\xd7\xe1\xda\xd8\xe5 \xd1\xe3\xdb\xde\xda,"
        b" \xd4\xd0 \xd2\xeb\xdf\xd5\xd9 \xe7\xd0\xee",
    },
    "Pijamalı hasta yağız şoföre çabucak güvendi": {
        UTF_8: b"Pijamal\xc4\xb1 hasta ya\xc4\x9f\xc4\xb1z"
        b" \xc5\x9fof\xc3\xb6re \xc3\xa7abucak g\xc3\xbcvendi",
        ISO8859_9: b"Pijamal\xfd hasta ya\xf0\xfdz"
        b" \xfeof\xf6re \xe7abucak g\xfcvendi",
    },
    "Høj bly gom vandt fræk sexquiz på wc": {
        UTF_8: b"H\xc3\xb8j bly gom vandt fr\xc3\xa6k sexquiz p\xc3\xa5 wc",
        ISO8859_10: b"H\xf8j bly gom vandt fr\xe6k sexquiz p\xe5 wc",
    },
    "Flygande bäckasiner söka hwila på mjuka tuvor": {
        UTF_8: b"Flygande b\xc3\xa4ckasiner s\xc3\xb6ka hwila p\xc3\xa5"
        b" mjuka tuvor",
        ISO8859_10: b"Flygande b\xe4ckasiner s\xf6ka hwila p\xe5"
        b" mjuka tuvor",
    },
    "Kæmi ný öxi hér, ykist þjófum nú bæði víl og ádrepa": {
        UTF_8: b"K\xc3\xa6mi n\xc3\xbd \xc3\xb6xi h\xc3\xa9r,"
        b" ykist \xc3\xbej\xc3\xb3fum n\xc3\xba b\xc3\xa6\xc3\xb0i"
        b" v\xc3\xadl og \xc3\xa1drepa",
        ISO8859_10: b"K\xe6mi n\xfd \xf6xi h\xe9r,"
        b" ykist \xfej\xf3fum n\xfa b\xe6\xf0i v\xedl og \xe1drepa",
    },
}


class GenericCallResult:

    """Result from a generic call"""

    def __init__(
        self,
        returncode: int = RETURNCODE_OK,
        stdout: str = "",
    ) -> None:
        """Internally store returncode and stdout"""
        self.__returncode = returncode
        self.__stdout = stdout

    @property
    def returncode(self) -> int:
        """returncode readonly attribute"""
        return self.__returncode

    @property
    def stdout(self) -> str:
        """stdout readonly attribute"""
        return self.__stdout

    @classmethod
    def do_call(cls, *args, **kwargs):
        """Abstract method: do the real function call"""
        raise NotImplementedError

    @classmethod
    def from_call(
        cls,
        *arguments,
        stdin_data=None,
        stdout=sys.stdout,
        stderr=sys.stderr,
        **kwargs,
    ):
        """Return a GenericCallResult instance
        from the real function call,
        mocking sys.stdin if stdin_data was provided.
        """
        assert stdout is sys.stdout
        assert stderr is sys.stderr
        if stdin_data is None:
            returncode = cls.do_call(*arguments, **kwargs)
        else:
            with patch("sys.stdin", new=io.StringIO(stdin_data)) as mock_stdin:
                assert mock_stdin is sys.stdin
                returncode = cls.do_call(*arguments, **kwargs)
            #
        #
        return cls(
            returncode=returncode,
            stdout=stdout.getvalue(),
        )


# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
