# -*- coding: utf-8 -*-

"""

qrstu.constants

Common constants


Copyright (C) 2023 Rainer Schwarzbach

This file is part of qrstu.

qrstu is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

qrstu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""


ASCII = "ascii"
CP1252 = "cp1252"
LATIN_1 = "latin_1"
UTF_8 = "utf-8"
UTF_8_SIG = "utf_8_sig"
UTF_16_BE = "utf_16_be"
UTF_16_LE = "utf_16_le"
UTF_32_BE = "utf_32_be"
UTF_32_LE = "utf_32_le"


# vim:fileencoding=utf-8 autoindent ts=4 sw=4 sts=4 expandtab:
